package ru.vkandyba.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.Domain;
import ru.vkandyba.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadDataCommand extends AbstractDataCommand{

    @NotNull
    @Override
    public String name() {
        return "backup-load-data";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Backup load data ...";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final File file = new File(BACKUP_FILE_NAME);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_FILE_NAME)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }
}
