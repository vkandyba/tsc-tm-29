package ru.vkandyba.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxBLoadJSONCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-jaxb-load-json";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data JaxB load JSON";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(ECLIPSELINK_MEDIA_TYPE, APPLICATION_JSON);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }
}
