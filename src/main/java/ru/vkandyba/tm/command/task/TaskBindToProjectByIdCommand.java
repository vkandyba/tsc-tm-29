package ru.vkandyba.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.entity.ProjectNotFoundException;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskBindToProjectByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "bind-task-to-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        @Nullable final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        @Nullable final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(userId, taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        @Nullable final Task task = serviceLocator.getProjectTaskService().bindTaskToProjectById(userId, projectId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
