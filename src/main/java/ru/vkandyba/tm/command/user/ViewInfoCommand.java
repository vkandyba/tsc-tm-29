package ru.vkandyba.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.model.User;

public class ViewInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "view-info";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show client info...";
    }

    @Override
    public void execute() {
        if (!serviceLocator.getAuthService().isAuth()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getAuthService().getUser();
        System.out.println("Login: " + user.getLogin());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Middle Name: " + user.getMiddleName());
        System.out.println("Last Name: " + user.getLastName());
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
