package ru.vkandyba.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class Backup extends Thread{

    @NotNull
    final Bootstrap bootstrap;

    private static final int INTERVAL = 3000;

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    @SneakyThrows
    @Override
    public void run() {
        while(true){
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init(){
        load();
        start();
    }

    public void save(){
        bootstrap.execute("backup-save-data");
    }

    public void load(){
        bootstrap.execute("backup-load-data");
    }

}
